#!/usr/bin/env python
# coding: utf-8

import argparse
from collections import Counter

import numpy as np
import pandas as pd

arg_parser = argparse.ArgumentParser(description='CSV parser')
arg_parser.add_argument('--path', '-p', type=str, help='path to csv file', required=True)
arg_parser.add_argument('--ranges', '-r', type=float, help='ranges to divide records to groups', default='0.05')
arg_parser.add_argument('--minimum_group', '-g', type=int, help='minimal amount of records per group', default='60')
arg_parser.add_argument('--minimum_speed', '-s', type=float, help='minimal cast speed', default='0.60')

param = ['Timestamp', 'P2010', 'P2012', 'P2014', 'P2034', 'P2036', 'P2038', 'P2044', 'P2046', 'P2048', 'P2050', 'P2052', 'P2054', 'P2058', 'P2060', 'P2062', 'P2064', 'P2066', 'P2068', 'P2070', 'P2072', 'P2074', 'P2076', 'P2078', 'P2080', 'P2094', 'P2096', 'P2098', 'P2100', 'P2102', 'P2104', 'P2106', 'P2108', 'P2110', 'P2112', 'P2114', 'P2118', 'P2120', 'P2122', 'P2124', 'P2126', 'P2128', 'P2130', 'P2132', 'P2134', 'P2136', 'P2138', 'P2140', 'P2142', 'P2144', 'P2146', 'P2148', 'P2150', 'P2152', 'P2154', 'P2156', 'P2158', 'P2160', 'P2162', 'P2164', 'P2166', 'P2168', 'P2170', 'P2172', 'P2174', 'P2176', 'P2178', 'P2180', 'P2182', 'P2184', 'P2186', 'P2188', 'P2192', 'P2194', 'P2426', 'P2427', 'P2433', 'P2434', 'P2435', 'P2440']


args=arg_parser.parse_args()
csv_path = args.path
minimum_cast_speed = args.minimum_speed - 0.01
group_range = args.ranges
minimum_occurences_in_group = args.minimum_group - 1

parameters_df = pd.read_csv(csv_path, names=param, parse_dates=['Timestamp'], encoding='utf_16_le')

#parameters_df.to_json(r'C:\Users\admin\PycharmProjects\algorytm_metale\test.json', orient='records', indent=1)

test_df = parameters_df.copy()

test_df['group'] = np.where(test_df.P2034 > minimum_cast_speed, test_df.P2034 // group_range, np.nan)

test_df.dropna(inplace=True)
# print(test_df.group.to_string(index=False))

# save_state = test_df.copy()

# test_df = save_state.copy()
previous_value = None
true_group = None
seen_groups = set()
for i, row in test_df.iterrows():
    group_id = row['group']
    if group_id != previous_value:  # group changed
        seen_groups.add(group_id)
        if group_id == true_group:  # checks if group is being reassigned new val
            test_df.at[i, 'group'] = previous_value
            # row['group'] = previous_value
        else:  # group not related to previous one
            true_group = group_id
            while group_id in seen_groups:
                group_id *= 10
            seen_groups.add(group_id)
            test_df.at[i, 'group'] = group_id

            # row['group'] = group_id
            previous_value = group_id

# remove records if their group doesnt reach threshold
test_df = test_df[test_df.groupby('group')['group'].transform('size') > minimum_occurences_in_group]

#test_df.to_json(r'C:\Users\admin\PycharmProjects\algorytm_metale\testdf.json', orient='records', indent=1)

keys = test_df.group.unique()
values = [str(int(x))[:2] for x in keys]  # recover original group assignment
values = [round(int(x) * group_range, 2) for x in values]  # get range group
occurances = []
strings = []
for x in values:
    occurances.append(x)
    occurance = Counter(occurances)[x]
    string = f'range {x}:{x + group_range}, #{occurance}'
    strings.append(string)
# print(values)
mapping = dict(zip(keys, strings))
# print(mapping)
test_df['display_string'] = test_df.group.map(mapping)
# test_df.to_json(r'C:\Users\admin\PycharmProjects\algorytm_metale\testdf_map.json', orient='records', indent=1)

# fixes groups to more logical number
mapping = {keys[i]: i for i in range(0, len(keys))}
test_df['group'] = test_df.group.map(mapping)
# print(test_df.group.value_counts())
test_df.to_json(r'.\output.json', orient='records', indent=1)
