from itertools import chain


def list_of_param():
    with open(r'parameters/param_col1.txt', 'r') as file1:
        col1 = file1.read()
    with open(r'parameters/param_col2.txt', 'r') as file2:
        col2 = file2.read()
    col1 = col1.split()
    col2 = col2.split()
    # a = list(map(list, zip(col1, col2)))
    # output = ['Timestamp'] + list(chain(*zip(col1, col2)))
    output = ['Timestamp'] + col2
    return output


if __name__ == '__main__':
    print(list_of_param())
