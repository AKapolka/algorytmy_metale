import argparse

arg_parser = argparse.ArgumentParser(description='CSV parser')
arg_parser.add_argument('--path','-p', type=str, help='path to csv file', required=True)
arg_parser.add_argument('--ranges','-r', type=float, help='ranges to divide records to groups', default='0.05')
arg_parser.add_argument('--minimum','-m', type=int, help='minimal amount of records per group', default='60')
arg_parser.add_argument('--minimum_speed','-s', type=float, help='minimal cast speed', default='0.60')

print(arg_parser.parse_args())